classDiagram
    Auto <|-- NákladníAuto
    Auto <|-- SportovníAuto
    Auto : +int Speed
    Auto : +String Barva
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class NákladníAuto{
      -int kapacita
    }
    class SportovníAuto{
      +bool má turbo
    }
